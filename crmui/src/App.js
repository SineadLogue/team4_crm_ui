import React from 'react';
import { Switch, Route } from "react-router-dom";
import AddCustomerClassComponent from './components/AddCustomerClassComponent'
import AddInteraction from './components/AddInteraction'
import ListCustomer from './components/ListCustomer';
import ListInteraction from './components/ListInteraction';
import Welcome from './components/Welcome';
import About from './components/About';
import Manager from './pages/Manager';
import CustomerContact from './pages/CustomerContact';
import {Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap';
import './css/custom.css';
import './App.css';
import AddCustomerV1 from "./components/AddCustomer";

function App() {
  return (
      <div id="container">
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="/home">GYM</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/home">Home</Nav.Link>
              <NavDropdown title="Customers" id="basic-nav-dropdown">
                <NavDropdown.Item href="/addCustomer">Add Customer</NavDropdown.Item>
                <NavDropdown.Item href="/customers">View Customers</NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="Customer Contacts" id="basic-nav-dropdown">
                <NavDropdown.Item href="/addInteraction">Add Customer Contact</NavDropdown.Item>
                <NavDropdown.Item href="/interactions">View All Customer Contacts</NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Form inline>
              <FormControl type="text" placeholder="Search" className="mr-sm-2" />
              <Button variant="outline-success">Search</Button>
            </Form>
          </Navbar.Collapse>
        </Navbar>
        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={Welcome} />
            <Route exact path="/customers" component={ListCustomer} />
            <Route exact path="/manager/customers" component={ListCustomer} />
            <Route exact path="/addCustomer" component={AddCustomerV1} />
            <Route exact path="/interactions" component={ListInteraction} />
            <Route exact path="/addInteraction" component={AddInteraction} />
            <Route  path="/manager/:customerId" component={Manager} />
            <Route  path="/customerContact/:customerId" component={CustomerContact} />
            <Route exact path="/about" component={About} />
          </Switch>
        </div>
      </div>
  );
}

export default App;
