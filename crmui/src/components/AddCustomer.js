
import React, {useState, useEffect} from 'react';
import {useDispatch, connect} from "react-redux";
import customerAdd from "../redux/actions/CustomerAddActions";
import customerList from "../redux/actions/CustomerListActions";
import '../css/custom.css'
import Redirect from "react-router-dom/es/Redirect";

//using Redux with React hooks instead of redux connect
function AddCustomer  (props)  {

    const dispatch = useDispatch();

    const initialFormState = {
        //id: 0,
        name: "",
        address: "",
        mobile: "",
        membershipLength: "",
        paymentType:""
         };

    const [customer, setCustomer] = useState(initialFormState);
    const handleInputChanges = (event) => {
        const { name, value } = event.target;
        setCustomer({ ...customer, [name]: value });
    };


    //const dispatch = useDispatch();

    // useEffect(() => {
    //     dispatch(customerList())},[]
    //
    // );

    console.log(props);

    // const initialFormState = {
    //     id: 0,
    //     name: "",
    //     address: "",
    //     mobile: "",
    //     membershipLength: "",
    //     paymentType:"",
    //     submitted:false };
    //
    // const [customer, setCustomer] = useState(initialFormState);


    // const handleInputChanges = (event) =>{
    //     const {name, value} = event.target;
    //     // console.log("handle input changes " + name + " " +  value);
    //     setCustomer({...customer, [name]: value});
    // };

    // const handleCustomerName = (event) =>{
    //     const {name, value} = event.target;
    //
    //     const customerMatch = props.customers.find(c => c.customerId == value);
    //     setInteraction({...interaction, customerName: customerMatch.name, [name]: value});
    //
    // }

    if (props.submitted){
        return (<Redirect to="customers"/>);
    }
    else {
        return (
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
            <form onSubmit={(event) => {
                event.preventDefault();
                //console.log("<<submit form: custid is " + interaction.interactionCustomerId);
                //console.log("<<submit form: cust name is " + interaction.customerName);
                dispatch(customerAdd(customer));
            }}>
                <h1>Add Customer</h1>
                <div className="form-group">
                    <label htmlFor="examplename">Name</label>
                    <input type="text" className="form-control" id="name" name="name" aria-describedby="nameHelp"
                           placeholder="Enter name" value={customer.name}  onChange={handleInputChanges} required/>
                    <small id="nameHelp" className="form-text text-muted">We'll never share your details with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputAddress">Address</label>
                    <input type="text" className="form-control" id="address" name="address" aria-describedby="addressHelp"
                           placeholder="Enter address" value={customer.address} onChange={handleInputChanges} required/>
                    <small id="addressHelp" className="form-text text-muted">We'll never share your details with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputMoile">Mobile</label>
                    <input type="text" className="form-control" id="mobile" name="mobile" aria-describedby="mobileHelp"
                           placeholder="Enter mobile" value={customer.mobile} onChange={handleInputChanges} required/>
                    <small id="mobileHelp" className="form-text text-muted">We'll never share your details with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="membershipLength">Membership Length</label>
                    <select name="membershipLength" id="membershipLength" name="membershipLength" className="form-control"
                            value={customer.membershipLength} onChange={handleInputChanges} required>
                        <option value="select">Select</option>
                        <option value="1">1 month</option>
                        <option value="6">6 months</option>
                        <option value="12">12 months</option>
                    </select>
                    <small id="membershipLengthHelp" className="form-text text-muted">We'll never share your details with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="paymentType">Payment Type</label>
                    <select name="paymentType" id="paymentType" name="paymentType" className="form-control"
                            value={customer.paymentType} onChange={handleInputChanges} required>
                        <option value="select">Select</option>
                        <option value="Direct Debit">Direct Debit</option>
                        <option value="Debit Card">Debit Card</option>
                        <option value="Credit Card">Credit Card</option>
                    </select>
                    <small id="paymentTypeHelp" className="form-text text-muted">We'll never share your details with anyone
                        else.</small>
                </div>
                <div className="form-group text-center">
                    <input type="submit" variant="outline-success" className="btn btn-success " value="Save"/>
                </div>
            </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        submitted: state.CustomerAddReducer.submitted
    }
}

const CustomersAdd = connect(mapStateToProps)(AddCustomer)
export default CustomersAdd
