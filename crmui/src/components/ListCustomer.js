import React, {Component} from 'react';
import CRMService from "../services/CRMServices";
import { Table } from 'react-bootstrap';

export default class  ListCustomers extends  Component {

    constructor(props) {
        super(props);
        this.state = {
            customers: [],
            currentIndex: -1}
    }

    retrieveCustomers() {
        CRMService.getAll()
            .then(response => {
                this.setState({
                    customers: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
        }

    componentDidMount() {
        this.retrieveCustomers();
    }


    renderCustomer(customer, index) {
        return (
            <tr key={index}>
                <td>{customer.name}</td>
                <td>{customer.address}</td>
                <td>{customer.mobile}</td>
                <td>{customer.membershipLength}</td>
                <td>{customer.paymentType}</td>
                <td>
                    <a href={`/Manager/${customer.customerId}`} className="btn btn-outline-success">
                        <span className="glyphicon glyphicon-pencil"></span> Update
                    </a>
                </td>
                <td>
                    <a href={`/CustomerContact/${customer.customerId}`} className="btn btn-outline-success">
                        <span className="glyphicon glyphicon-pencil"></span> View Customer Contact
                    </a>
                </td>
            </tr>
        )
    }

    render(){
        const {customers,currentIndex} = this.state;
        return  (
            <div className="col-md-12">
                <h1>Customers List</h1>

                <Table responsive="sm" striped condensed hover>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Mobile</th>
                        <th>Membership Length</th>
                        <th>Payment Type</th>
                    </tr>
                    </thead>
                    <tbody>
                        {customers.map(this.renderCustomer)}
                    </tbody>
                </Table>
            </div>
        )


    }


}