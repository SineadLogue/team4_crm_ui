import React from 'react';

function Welcome() {
    return (
        <div className="text-center">
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
                    <h1 className="display-4">Allstate Dodgeball Gym</h1>
                    <p className="lead">"If you can dodge a wrench, you can dodge a ball" - Patches O'Houlihan</p>
                </div>
                <div>
                    <img src={require("../images/dodgeball.jpg")} className="rounded" alt="Dodgeball" width="460" height="345" />
                </div>
            </div>
        </div>
    );
}



export default Welcome;