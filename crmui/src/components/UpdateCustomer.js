import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";
import customerUpdate from "../redux/actions/CustomerUpdateActions";
import {connect, useDispatch, useSelector} from "react-redux";
import Redirect from "react-router-dom/es/Redirect";

function UpdateCustomer(props) {
    const dispatch = useDispatch();
    const [customer, setCustomer] = useState(props.customer)

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setCustomer({ ...customer, [name]: value });
    };

    useEffect(
        () => {
            setCustomer(props.customer)
        },
        [ props ]
    )
    console.log(props);
    if (props.submitted) {
        return <Redirect to="customers" />
    }
    else if (customer) {

        return (<form
                onSubmit={(event) => {
                    event.preventDefault();
                    dispatch(customerUpdate(customer));
                }}>
                <h1>Update Customer</h1>
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control" id="name" name="name" aria-describedby="nameHelp"
                           placeholder="Enter name" value={customer.name} required onChange={handleInputChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="address">Address</label>
                    <input type="text" className="form-control" id="address" name="address" aria-describedby="addressHelp"
                           placeholder="Enter address" value={customer.address} required onChange={handleInputChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="mobile">Mobile</label>
                    <input type="text" className="form-control" id="mobile" name="mobile"
                           aria-describedby="mobileHelp"
                           placeholder="Enter mobile" value={customer.mobile} required onChange={handleInputChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="membershipLength">Membership Length</label>
                    <select name="membershipLength" id="membershipLength" className="form-control"
                            value={customer.membershipLength} onChange={handleInputChange} required>
                        <option value="select">Select</option>
                        <option value="1">1 month</option>
                        <option value="6">6 months</option>
                        <option value="12">12 months</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="paymentType">Payment Type</label>
                    <select name="paymentType" id="paymentType" className="form-control"
                            value={customer.paymentType} onChange={handleInputChange} required>
                        <option value="select">Select</option>
                        <option value="Direct Debit">Direct Debit</option>
                        <option value="Debit Card">Debit Card</option>
                        <option value="Credit Card">Credit Card</option>
                    </select>
                </div>
                <div className="form-group">
                    <input type="submit" className="form-control" value="Save"></input>
                </div>
            </form>
        )
    } else {
        return (<h1>Update</h1>)
    }
}

const mapStateToProps = (state) => {
    return {
        customer: state.CustomerGetReducer.customer,
        submitted: state.CustomerUpdateReducer.submitted
    }
}

const UpdateCustomerConnected = connect(mapStateToProps)(UpdateCustomer)
// export default CustomerItem
UpdateCustomer.propTypes = {
    customer: PropTypes.object.isRequired,
};

export default UpdateCustomerConnected