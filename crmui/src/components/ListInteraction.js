import React, {useEffect} from "react";
import {connect, useDispatch} from 'react-redux'
import interactionList from "../redux/actions/InteractionListActions";
import Button from "react-bootstrap/Button";
import {Table} from "react-bootstrap";


//useEffects does the job of componentDidMount, componentDidUpdate, componentWillUpdate combined
const  Interactions= (props) => {
    const dispatch =useDispatch();

    useEffect(() => {
            dispatch(interactionList())},
        []
    );

    return (
        <div className="col-md-12">
            <h1>Customer Contact List</h1>

            <Button  className="btn btn-success " onClick={()=>dispatch(interactionList())} >Refresh</Button>
            <Table responsive="sm" striped condensed hover>
                <thead>
                    <tr>
                        <th>Customer</th>
                        <th>Source</th>
                        <th>Query Type</th>
                        <th>Other Information</th>
                    </tr>
                </thead>
                <tbody>
                {props.interactions && props.interactions.map(el => (

                    <tr key={el.id}>
                        {/*<td>{el.customerName}</td>*/}
                        <td>{el.customerName}</td>
                        <td>{el.source}</td>
                        <td>{el.queryType}</td>
                        <td>{el.otherInformation}</td>
                        {/*<td><a href={`/manager/${el.id}`}>Edit</a></td>*/}
                    </tr>
                    ))
                }
                </tbody>
            </Table>
        </div>
    )}

const mapStateToProps = (state) => {
    return {
        interactions: state.InteractionListReducer.interactions,
        pending: state.InteractionListReducer.pending
    }
}

// connect  component
const List = connect(mapStateToProps)(Interactions)
export default List