
import React, {useState, useEffect} from 'react';
import {useDispatch, connect} from "react-redux";
import interactionAdd from "../redux/actions/InteractionAddActions";
import customerList from "../redux/actions/CustomerListActions";
import '../css/custom.css'
import Redirect from "react-router-dom/es/Redirect";

//using Redux with React hooks instead of redux connect
const AddInteraction = (props) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(customerList())},[]

    );

    console.log(props.customers);

    const initialFormState = {
        id: 0,
        interactionCustomerId: 0,
        customerName: '' ,
        source: "",
        queryType: "",
        otherInformation:"",
        submitted:false };

    const [interaction, setInteraction] = useState(initialFormState);


    const handleInputChanges = (event) =>{
        const {name, value} = event.target;
        // console.log("handle input changes " + name + " " +  value);
        setInteraction({...interaction, [name]: value});
    };

    const handleCustomerName = (event) =>{
        const {name, value} = event.target;

        const customerMatch = props.customers.find(c => c.customerId == value);
        setInteraction({...interaction, customerName: customerMatch.name, [name]: value});

    }

    if (props.submitted){
        return (<Redirect to="interactions"/>);
    }
    else {
        return (
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
            <form onSubmit={(event) => {
                event.preventDefault();
                console.log("<<submit form: custid is " + interaction.interactionCustomerId);
                console.log("<<submit form: cust name is " + interaction.customerName);
                dispatch(interactionAdd(interaction));
            }}>
                <h1 className="PageHeader"> Add Customer Contact</h1>
                <div className="form-group">
                    <label htmlFor="customerName">Customer</label>
                    <select className="form-control" name="interactionCustomerId" id="interactionCustomerId"
                            placeholder="Choose Customer"
                            value={interaction.interactionCustomerId} onChange={handleCustomerName}>
                        <option key={0} value="0" selected>Please choose customer</option>
                        {props.customers.map(el => (
                            <option key={el.customerId} value={el.customerId}>{el.name}</option>
                        ))}
                    </select>
                    <small id="nameHelp" className="form-text text-muted">Customer name is private.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="interactionSources">Source</label>
                    <input list="interactionSources" className="form-control" id="source" name="source"
                           value={interaction.source} aria-describedby="sourceHelp"
                           placeholder="Enter Customer Contact Source" required onChange={handleInputChanges}/>
                    <small id="sourceHelp" className="form-text text-muted">Initial Contact Source</small>
                    <datalist id="interactionSources">
                        <option value="Phone Call"/>
                        <option value="Text"/>
                        <option value="Facebook"/>
                        <option value="Face to Face"></option>
                    </datalist>
                </div>
                <div className="form-group">
                    <label htmlFor="queryType">Query Type</label>
                    <input list="queryTypes" id="queryType" name="queryType" aria-describedby="queryTypeHelp"
                           vlue={interaction.queryType}
                           placeholder="Enter Query Type" required className="form-text form-control"
                           onChange={handleInputChanges}/>
                    <small id="queryTypeHelp" className="form-text text-muted">Choose the closest match</small>
                    <datalist id="queryTypes">
                        <option value="Membership Cost"/>
                        <option value="Class Sign-up"/>
                        <option value="Personal Trainer"/>
                        <option value="Referrals"></option>
                    </datalist>
                </div>
                <div className="form-group">
                    <label htmlFor="otherInformation">Other Information</label>
                    <input type="text" id="otherInformation" name="otherInformation" aria-describedby="otherInfoHelp"
                           value={interaction.otherInformation}
                           className="form-text form-control" required onChange={handleInputChanges}/>
                    <small id="otherInfoHelp" className="form-text text-muted">Please add any relevant
                        information</small>
                </div>


                <div className="form-group text-center">
                    <input type="submit" variant="outline-success" className="btn btn-success " value="Save"/>
                </div>
            </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        customers: state.CustomerListReducer.customers,
        submitted: state.InteractionAddReducer.submitted
    }
}

const InteractionsAdd = connect(mapStateToProps)(AddInteraction)
export default InteractionsAdd
