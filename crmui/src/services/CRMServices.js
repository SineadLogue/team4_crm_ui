import http from "../http-common";

export class CRMService {

    getStatus() {
        return http.get("/status");
    }

    getAll() {
        return http.get("/getAllCustomers");
    }

    get(id) {
        return http.get(`/id/${id}`);
    }

    save(data) {
        return http.post("/addCustomer", data);
    }

    addInteraction (data) {
        return http.post("/addInteraction", data);
    }

    getAllInteractions(){
        return http.get("/getAllInteractions");
    }

    getCustomerInteractions(id){
        return http.get(`/getCustomerInteractions/${id}`);
    }

    updateCustomer(data){
        return http.post("/updateCustomer", data);
    }
}

export default new CRMService();