import {applyMiddleware,  createStore, combineReducers} from "redux";
import InteractionAddReducer from "./reducers/InteractionAddReducer";
import CombineReducer from "./reducers/CombineReducer";
import thunk from "redux-thunk";

const initialState = {
    interactions: [],
    //customers:[]

    customers:[
        {name: 'Hermione Granger',
            address: '123 Main Street',
            membershipLength: 'Annual',
            paymentType: 'Direct Debit'},
        {name: 'Seamus Finnegan',
            address: '123 Main Street',
            membershipLength: 'Monthly',
            paymentType: 'Direct Debit'}


    ]
};
export const middlewares = [thunk];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ //|| compose;

export const store = createStore(
    CombineReducer, //typically combine all reducers
    //initialState,
    //  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    composeEnhancer(applyMiddleware(...middlewares))
);
//console.log("store:"+ store.getState())
//store.dispatch({type:'CUSTOMER_LIST_REQUEST'})
//console.log("store:" + store.getState())


//export default store;