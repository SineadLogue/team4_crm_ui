
import {CUSTOMER_ADD_REQUEST , CUSTOMER_ADD_SUCCESS , CUSTOMER_ADD_FAIL}
    from "../constants/CRMConstants"



import CRMServices from "../../services/CRMServices";

const customerAdd = (customer) => async  (dispatch) => {
    console.log("<<<<<<<Interaction Add action <<<<<");
    try {
        dispatch({type: CUSTOMER_ADD_REQUEST, payload: customer});
        const newCustomer= await CRMServices.save(customer);
        dispatch({type: CUSTOMER_ADD_SUCCESS, payload: newCustomer});
    } catch (error) {
        dispatch({type: CUSTOMER_ADD_FAIL, payload: error});
    }
};


export default  customerAdd;