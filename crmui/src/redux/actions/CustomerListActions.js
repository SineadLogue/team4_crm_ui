
import {CUSTOMER_LIST_REQUEST , CUSTOMER_LIST_SUCCESS , CUSTOMER_LIST_FAIL}
    from "../constants/CRMConstants"

import CRMServices from "../../services/CRMServices";

const customerList = () =>  (dispatch) => {
    try {
        console.log("<<<<<<<in customerListAction <<<<<<<<<<<<<<<<")
        dispatch({type: CUSTOMER_LIST_REQUEST});
        const data = CRMServices.getAll().then(response => {
            dispatch({type: CUSTOMER_LIST_SUCCESS, payload: response.data})
        });
    } catch (error) {
        dispatch({type: CUSTOMER_LIST_FAIL, payload: error});
    }
};



export default customerList;