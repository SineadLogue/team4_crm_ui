
import {INTERACTION_LIST_REQUEST , INTERACTION_LIST_SUCCESS , INTERACTION_LIST_FAIL}
    from "../constants/CRMConstants"

import CRMServices from "../../services/CRMServices";

const customerInteractionList = (id) =>  (dispatch) => {
    try {
        console.log("<<<<<<<in interactionListAction <<<<<<<<<<<<<<<<")
        console.log("id is;" + id)
        dispatch({type: INTERACTION_LIST_REQUEST});
        const data = CRMServices.getCustomerInteractions(id).then(response => {
            dispatch({type: INTERACTION_LIST_SUCCESS, payload: response.data})
        });
    } catch (error) {
        dispatch({type: INTERACTION_LIST_FAIL, payload: error});
    }
};



export default  customerInteractionList;