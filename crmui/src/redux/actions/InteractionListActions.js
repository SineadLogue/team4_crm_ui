
import {INTERACTION_LIST_REQUEST , INTERACTION_LIST_SUCCESS , INTERACTION_LIST_FAIL}
    from "../constants/CRMConstants"

import CRMServices from "../../services/CRMServices";

const interactionList = () =>  (dispatch) => {
    try {
        console.log("<<<<<<<in interactionListAction <<<<<<<<<<<<<<<<")
        dispatch({type: INTERACTION_LIST_REQUEST});
        const data = CRMServices.getAllInteractions().then(response => {
            dispatch({type: INTERACTION_LIST_SUCCESS, payload: response.data})
        });
    } catch (error) {
        dispatch({type: INTERACTION_LIST_FAIL, payload: error});
    }
};



export default  interactionList;