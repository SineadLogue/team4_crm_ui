
import {CUSTOMER_UPDATE_REQUEST , CUSTOMER_UPDATE_SUCCESS , CUSTOMER_UPDATE_FAIL}
    from "../constants/CRMConstants"

import CRMServices from "../../services/CRMServices";

const customerUpdate = (customer) => async (dispatch) => {
    try {
        dispatch({ type: CUSTOMER_UPDATE_REQUEST, payload: customer });

        const updateCustomer = await CRMServices.updateCustomer(customer);

        dispatch({ type:CUSTOMER_UPDATE_SUCCESS, payload: updateCustomer });
    } catch (error) {
        dispatch({ type: CUSTOMER_UPDATE_FAIL, payload: error });
    }
};

export default customerUpdate;