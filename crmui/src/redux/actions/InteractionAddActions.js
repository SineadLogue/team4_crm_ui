
import {INTERACTION_ADD_REQUEST , INTERACTION_ADD_SUCCESS , INTERACTION_ADD_FAIL}
    from "../constants/CRMConstants"

import CRMServices from "../../services/CRMServices";

const interactionAdd = (interaction) => async  (dispatch) => {
    console.log("<<<<<<<Interaction Add action <<<<<");
    try {
        dispatch({type: INTERACTION_ADD_REQUEST, payload: interaction});
        const newInteraction = await CRMServices.addInteraction(interaction);
        dispatch({type: INTERACTION_ADD_SUCCESS, payload: newInteraction});
    } catch (error) {
        dispatch({type: INTERACTION_ADD_FAIL, payload: error});
    }
};




export default  interactionAdd;