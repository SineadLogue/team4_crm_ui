import {CUSTOMER_GET_REQUEST , CUSTOMER_GET_SUCCESS , CUSTOMER_GET_FAIL}
    from "../constants/CRMConstants"
import CRMServices from "../../services/CRMServices";

const customerGet = (id) =>  (dispatch) => {
    try {
        console.log(">>> get in dispatch ");
        dispatch({ type: CUSTOMER_GET_REQUEST });
        const data =  CRMServices.get(id).then(response => {
            dispatch({ type: CUSTOMER_GET_SUCCESS, payload: response.data });
        });

        console.log(">>> CUSTOMER get= + got data");

    } catch (error) {
        console.log(">>> in dispatch error=" + error);
        dispatch({ type: CUSTOMER_GET_FAIL, payload: error });
    }
};

export default customerGet;