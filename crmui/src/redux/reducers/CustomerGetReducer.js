import {CUSTOMER_GET_REQUEST , CUSTOMER_GET_SUCCESS , CUSTOMER_GET_FAIL}
    from "../constants/CRMConstants"

const customerGetInitialState = {
    customer: {},
    error: "",
    loading: false,
};

function CustomerGetReducer(
    state = customerGetInitialState,
    action
) {
    switch (action.type) {
        case CUSTOMER_GET_REQUEST:
            return { ...state,loading: false };
        case CUSTOMER_GET_SUCCESS:
            return { ...state, loading: true, customer: action.payload };
        case CUSTOMER_GET_FAIL:
            return { ...state, loading: false, error: action.payload };
        default:
            return state;
    }
}

export default CustomerGetReducer;