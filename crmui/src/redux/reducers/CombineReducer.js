import { combineReducers } from "redux";
import CustomerAddReducer from "./CustomerAddReducer";
import CustomerGetReducer from "./CustomerGetReducer";
import CustomerListReducer from "./CustomerListReducer";
import CustomerUpdateReducer from "./CustomerUpdateReducer";
import InteractionListReducer from "./InteractionListReducer";
import InteractionAddReducer from "./InteractionAddReducer";


export default combineReducers({
    CustomerAddReducer:CustomerAddReducer,
    CustomerGetReducer:CustomerGetReducer,
    CustomerListReducer:CustomerListReducer,
    CustomerUpdateReducer:CustomerUpdateReducer,
    InteractionListReducer:InteractionListReducer,
    InteractionAddReducer:InteractionAddReducer
})