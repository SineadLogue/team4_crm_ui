import {CUSTOMER_LIST_REQUEST , CUSTOMER_LIST_SUCCESS , CUSTOMER_LIST_FAIL}
    from "../constants/CRMConstants"


//Payload is what is bundled in your actions and passed around between reducers in your redux application.
// //For example - const someAction = { type: "Test", payload: {user: "Test User", age: 25}, }
const customersInitialState = {
    pending: false,
    customers:[],
    error: null
}

function CustomerListReducer(state=customersInitialState,action)
{
    console.log("Customer List Reducer action type = " + action.type);
    console.log("Customer list reducer: " + customersInitialState.customers)
    switch (action.type) {
        case CUSTOMER_LIST_REQUEST:
            return { ...state, pending: true };
        case CUSTOMER_LIST_SUCCESS:
            console.log("<<<reducer action: " + action.payload);
            return { ...state, pending: false, customers: action.payload };
        case CUSTOMER_LIST_FAIL:
            return { ...state, pending: false, error: action.payload };

        default:
            return state;
    }
}

export default CustomerListReducer;
