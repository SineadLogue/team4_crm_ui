
import {CUSTOMER_ADD_REQUEST , CUSTOMER_ADD_SUCCESS , CUSTOMER_ADD_FAIL}
    from "../constants/CRMConstants"

const  customerInitialState = {
    customer: {},
    error: "",
    submitted: false,
};

function CustomerAddReducer(
    state = customerInitialState,
    action){
    switch (action.type){
        case CUSTOMER_ADD_REQUEST:
            return {...state, customer: action.payload};
        case CUSTOMER_ADD_SUCCESS:
            return {...state, submitted:true, customer:  action.payload};
        case CUSTOMER_ADD_FAIL:
            return { ...state, submitted: false, error: action.payload};
        default:
            return state;

    }
}

export default CustomerAddReducer

