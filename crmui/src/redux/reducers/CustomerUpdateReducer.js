import {CUSTOMER_UPDATE_REQUEST , CUSTOMER_UPDATE_SUCCESS , CUSTOMER_UPDATE_FAIL}
    from "../constants/CRMConstants"

const customerUpdateInitialState = {
    customer: {},
    error: "",
    loading: false,
    submitted: false
};

function CustomerUpdateReducer(
    state = customerUpdateInitialState,
    action
) {
    switch (action.type) {
        case CUSTOMER_UPDATE_REQUEST:
            return { ...state, customer: action.payload };
        case CUSTOMER_UPDATE_SUCCESS:
            return { ...state, submitted: true, customer: action.payload };
        case CUSTOMER_UPDATE_FAIL:
            return { ...state, submitted: false, error: action.payload };
        default:
            return state;
    }
}

export default CustomerUpdateReducer;
