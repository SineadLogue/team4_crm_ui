import {INTERACTION_LIST_REQUEST , INTERACTION_LIST_SUCCESS , INTERACTION_LIST_FAIL}
from "../constants/CRMConstants"


//Payload is what is bundled in your actions and passed around between reducers in your redux application.
// //For example - const someAction = { type: "Test", payload: {user: "Test User", age: 25}, }
const interactsInitialState = {
    pending: false,
    interactions: [],
    error: null
}

function InteractionListReducer(state=interactsInitialState,action)
{
    console.log("Interaction List Reducer action type = " + action.type);

    switch (action.type) {
        case INTERACTION_LIST_REQUEST:
            return { ...state, pending: true };
        case INTERACTION_LIST_SUCCESS:
            console.log("<<<reducer action: " + action.payload);
            return { ...state, pending: false, interactions: action.payload };
        case INTERACTION_LIST_FAIL:
            return { ...state, pending: false, error: action.payload };

        default:
            return state;
    }
}

export default InteractionListReducer;
