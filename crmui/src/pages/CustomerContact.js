import React, {useEffect} from "react";
import {connect, useDispatch} from 'react-redux'
import Button from "react-bootstrap/Button";
import customerInteractionList from "../redux/actions/CustomerInteractionListActions";



//useEffects does the job of componentDidMount, componentDidUpdate, componentWillUpdate combined
const  Interactions= (props) => {
    const dispatch =useDispatch();
    const customerId = props.match.params.customerId;

    console.log("Customer ID is:-" + customerId);

    useEffect(() => {
            dispatch(customerInteractionList(customerId))},
        []
    );


    return (

        <div>
            <h1>Customer Contact Details</h1>

            <Button  className="btn btn-success" onClick={()=>dispatch(customerInteractionList(this.customerId))} >Refresh</Button>

            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Customer</th>
                    <th>Source</th>
                    <th>Query Type</th>
                    <th>Other Information</th>
                </tr>
                </thead>
                {props.interactions && props.interactions.map(el => (
                    <tr key={el.id}>
                        <td>{el.customerName}</td>
                        <td>{el.source}</td>
                        <td>{el.queryType}</td>
                        <td>{el.otherInformation}</td>
                        {/*<td><a href={`/manager/${el.id}`}>Edit</a></td>*/}
                    </tr>))}
            </table>
        </div>
    )}

const mapStateToProps = (state) => {
    return {
        interactions: state.InteractionListReducer.interactions,
        pending: state.InteractionListReducer.pending
    }
}

// connect  component
const List = connect(mapStateToProps)(Interactions)
export default List