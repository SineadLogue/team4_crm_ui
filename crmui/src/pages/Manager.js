import React, {useEffect} from 'react';
import AddCustomerClassComponent from "../components/AddCustomerClassComponent";
import {useDispatch, useSelector} from "react-redux";
import UpdateCustomer from "../components/UpdateCustomer";
import customerGet from "../redux/actions/CustomerGetActions";


function Manager(props) {
    const customerid = props.match.params.customerId;
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    const customer = state.CustomerGetReducer.customer;

    useEffect(() => {
        if (customerid) {
            dispatch(customerGet(customerid));
        }
        return () => {};
    }, []);

    if (customerid == "" || !customerid) {
        return (
            <AddCustomerClassComponent></AddCustomerClassComponent>
        );
    } else {
        return (
            <UpdateCustomer customer={customer}></UpdateCustomer>
        );
    }

}
export default Manager;